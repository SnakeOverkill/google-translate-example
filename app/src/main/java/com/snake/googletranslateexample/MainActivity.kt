package com.snake.googletranslateexample

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.google.cloud.translate.Translate
import com.google.cloud.translate.TranslateOptions
import com.snake.googletranslateexample.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Locale

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val API_KEY = "AIzaSyBKQnKlCr7neQPQ0SymkS6c54PS8Xz77Rg"
    private val TARGET_LANGUAGE = "vi"
    private val SOURCE_LANGUAGE = "en"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.btnTranslate.setOnClickListener {
            val value = binding.edtTranslate.text.toString()
            translate(value)
        }

        binding.ivVoiceTranslate.setOnClickListener {
            speechToText()
        }

    }

    private fun translate(value: String) {
        if (value.isNotEmpty()) {
            binding.btnTranslate.loading()
            CoroutineScope(Dispatchers.IO).launch {
                val translate = TranslateOptions.newBuilder().setApiKey(API_KEY).build().service
                val translation = translate.translate(
                    value,
                    Translate.TranslateOption.targetLanguage(TARGET_LANGUAGE),
                    Translate.TranslateOption.sourceLanguage(SOURCE_LANGUAGE)
                )
                runOnUiThread {
                    binding.btnTranslate.reset()
                    binding.tvTranslated.text = translation.translatedText
                }
            }
        } else {
            Toast.makeText(this@MainActivity, "Please Enter Text Before Translate!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun speechToText() {
        try {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH)
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Please Start Speaking")
            intentLauncher.launch(intent)
        } catch (e: Exception) {
            Toast.makeText(this, "Something Went Wrong", Toast.LENGTH_SHORT).show()
        }
    }

    private val intentLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            try {
                val speechResult = result.data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)?.get(0) ?: ""
                translate(speechResult)
            } catch (e: Exception) {
                e.stackTrace
            }
        }
    }



}